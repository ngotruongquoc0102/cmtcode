﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListSV.DAL
{
    public class DataProvider
    {
        private static DataProvider _Instance;
        //khai báo chuỗi kết nối DB
        private string strcnn = @"Data Source=DESKTOP-5E2U87G;Initial Catalog=SvDB;Integrated Security=True;Connect Timeout=30";

        public static DataProvider Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new DataProvider();
                return _Instance;
            }
            private set => _Instance = value;
        }
        private DataProvider()
        {

        }
        public DataTable GetRecord(string query)
        {
            //Khởi tạo đối tượng dt có kiểu dữ liệu là dataTable
            DataTable dt = new DataTable();
            try
            {
                //Khởi tạo đối tương SqlConnection với tham số truyền vào là chuỗi kết nối Database khai báo ở trên
                using (SqlConnection cnn = new SqlConnection(strcnn))
                {
                    //khởi tạo đối tượng da có kiểu dữ liệu là SqlDataAdapter SqlDataAdapter là cầu nối trung gian giữa Datatable và DB
                    SqlDataAdapter da = new SqlDataAdapter(query, cnn);
                    //Fill dữ liệu từ da đổ vào DataTable khi ngắt kết nối DB dataAdapter mất Datatable vẫn còn 
                    da.Fill(dt);
                }
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool ExecuteDB(string query, params object[] data) 
        {
            try
            {
                //sử dụng using để các biến chỉ khởi tạo trong cục bộ using sau using vùng nhớ giải phóng 
                //Khởi tạo đối tương SqlConnection với tham số truyền vào là chuỗi kết nối Database khai báo ở trên
                using (SqlConnection cnn = new SqlConnection(strcnn))
                {
                    //Khởi tạo Sqlcommand để thực thi câu lệnh sql với 2 tham số query và cnn
                    SqlCommand cmd = new SqlCommand(query, cnn);
                    //kiểm tra trong chuỗi câu truy vấn nếu có từ insert hay update thì thực thi câu lệnh (đúng trả về index >=0 sai inde = -1)
                    if (query.IndexOf("insert") != -1 || query.IndexOf("update") != -1)
                    {
                        //khai báo đối tượng Sqlparameter @ là tên tham số ứng với từng tham số truyền trong câu lệnh typeof là kiểu dữ liệu của params
                        SqlParameter MSSV = new SqlParameter("@MSSV", typeof(string));
                        SqlParameter NameSV = new SqlParameter("@NameSV", typeof(string));
                        SqlParameter Gender = new SqlParameter("@Gender", typeof(int));
                        SqlParameter Birthday = new SqlParameter("@Birthday", typeof(DateTime));
                        SqlParameter DiaChi = new SqlParameter("@DiaChi", typeof(string));
                        SqlParameter Phone = new SqlParameter("@Phone", typeof(string));
                        SqlParameter LopHP = new SqlParameter("@LopHP", typeof(string));
                        //data là mảng các đối tượng data[0] tương ứng MSSV data[1] tương ứng nameSV
                        MSSV.Value = data[0];
                        NameSV.Value = data[1];
                        Gender.Value = Convert.ToInt32(data[2]);
                        Birthday.Value = data[3];
                        DiaChi.Value = data[4];
                        Phone.Value = data[5];
                        LopHP.Value = data[6];
                        //add param vào đối tượng SqlCommand sử dụng addRange add mảng các đối tượng Sqlparamter
                        cmd.Parameters.AddRange(new SqlParameter[]
                        {
                            MSSV,
                            NameSV,
                            Gender,
                            Birthday,
                            DiaChi,
                            Phone,
                            LopHP,
                         });
                    }
                    //kiểm tra trong chuỗi câu truy vấn nếu có từ delete
                    if (query.IndexOf("delete") != -1)
                    {
                        SqlParameter MSSV = new SqlParameter("@MSSV", typeof(string));
                        //chỉ lấy MSSV vì xóa theo mssv
                        MSSV.Value = data[0];
                        cmd.Parameters.Add(MSSV);
                    }
                    // khi thực thi câu lệnh query hay nonquery phải có open và close
                    cnn.Open();
                    int n = (int)cmd.ExecuteNonQuery();  //NonQuery sử dụng cho insert update, delete câu lệnh làm thay đổi DB
                    cnn.Close();
                    return true;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }
    }
}
