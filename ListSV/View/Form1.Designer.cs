﻿namespace ListSV
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Gender = new System.Windows.Forms.GroupBox();
            this.Female = new System.Windows.Forms.RadioButton();
            this.Male = new System.Windows.Forms.RadioButton();
            this.diachi = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.Phone = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxLopHp = new System.Windows.Forms.ComboBox();
            this.name = new System.Windows.Forms.TextBox();
            this.mssv = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.sortProperty = new System.Windows.Forms.ComboBox();
            this.sort = new System.Windows.Forms.Button();
            this.del = new System.Windows.Forms.Button();
            this.update = new System.Windows.Forms.Button();
            this.add = new System.Windows.Forms.Button();
            this.DataSV = new System.Windows.Forms.DataGridView();
            this.search = new System.Windows.Forms.Button();
            this.search_input = new System.Windows.Forms.TextBox();
            this.lopHp = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.Gender.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataSV)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Gender);
            this.groupBox1.Controls.Add(this.diachi);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.Phone);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.comboBoxLopHp);
            this.groupBox1.Controls.Add(this.name);
            this.groupBox1.Controls.Add(this.mssv);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(26, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1082, 244);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin";
            // 
            // Gender
            // 
            this.Gender.Controls.Add(this.Female);
            this.Gender.Controls.Add(this.Male);
            this.Gender.Location = new System.Drawing.Point(871, 47);
            this.Gender.Name = "Gender";
            this.Gender.Size = new System.Drawing.Size(187, 145);
            this.Gender.TabIndex = 12;
            this.Gender.TabStop = false;
            this.Gender.Text = "Gender";
            // 
            // Female
            // 
            this.Female.AutoSize = true;
            this.Female.Location = new System.Drawing.Point(21, 98);
            this.Female.Name = "Female";
            this.Female.Size = new System.Drawing.Size(87, 24);
            this.Female.TabIndex = 1;
            this.Female.TabStop = true;
            this.Female.Text = "Female";
            this.Female.UseVisualStyleBackColor = true;
            // 
            // Male
            // 
            this.Male.AutoSize = true;
            this.Male.Location = new System.Drawing.Point(21, 42);
            this.Male.Name = "Male";
            this.Male.Size = new System.Drawing.Size(68, 24);
            this.Male.TabIndex = 0;
            this.Male.TabStop = true;
            this.Male.Text = "Male";
            this.Male.UseVisualStyleBackColor = true;
            // 
            // diachi
            // 
            this.diachi.Location = new System.Drawing.Point(505, 158);
            this.diachi.Name = "diachi";
            this.diachi.Size = new System.Drawing.Size(206, 26);
            this.diachi.TabIndex = 11;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(505, 109);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(301, 26);
            this.dateTimePicker1.TabIndex = 10;
            // 
            // Phone
            // 
            this.Phone.Location = new System.Drawing.Point(503, 47);
            this.Phone.Name = "Phone";
            this.Phone.Size = new System.Drawing.Size(206, 26);
            this.Phone.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(421, 167);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 20);
            this.label6.TabIndex = 8;
            this.label6.Text = "Diachi";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(421, 109);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 20);
            this.label5.TabIndex = 7;
            this.label5.Text = "Ngày sinh";
            // 
            // comboBoxLopHp
            // 
            this.comboBoxLopHp.FormattingEnabled = true;
            this.comboBoxLopHp.Items.AddRange(new object[] {
            "17T1",
            "17T2",
            "17T3",
            "17TCLC1",
            "17TCLC2"});
            this.comboBoxLopHp.Location = new System.Drawing.Point(95, 164);
            this.comboBoxLopHp.Name = "comboBoxLopHp";
            this.comboBoxLopHp.Size = new System.Drawing.Size(249, 28);
            this.comboBoxLopHp.TabIndex = 6;
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(95, 106);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(206, 26);
            this.name.TabIndex = 5;
            // 
            // mssv
            // 
            this.mssv.Location = new System.Drawing.Point(95, 50);
            this.mssv.Name = "mssv";
            this.mssv.Size = new System.Drawing.Size(206, 26);
            this.mssv.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(421, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Phone";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 164);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Lớp HP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Họ Tên";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "MSSV";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.sortProperty);
            this.groupBox2.Controls.Add(this.sort);
            this.groupBox2.Controls.Add(this.del);
            this.groupBox2.Controls.Add(this.update);
            this.groupBox2.Controls.Add(this.add);
            this.groupBox2.Controls.Add(this.DataSV);
            this.groupBox2.Controls.Add(this.search);
            this.groupBox2.Controls.Add(this.search_input);
            this.groupBox2.Controls.Add(this.lopHp);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(26, 294);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1082, 349);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Danh sách SV";
            // 
            // sortProperty
            // 
            this.sortProperty.FormattingEnabled = true;
            this.sortProperty.Items.AddRange(new object[] {
            "Name"});
            this.sortProperty.Location = new System.Drawing.Point(745, 308);
            this.sortProperty.Name = "sortProperty";
            this.sortProperty.Size = new System.Drawing.Size(215, 28);
            this.sortProperty.TabIndex = 20;
            // 
            // sort
            // 
            this.sort.Location = new System.Drawing.Point(570, 301);
            this.sort.Name = "sort";
            this.sort.Size = new System.Drawing.Size(118, 43);
            this.sort.TabIndex = 19;
            this.sort.Text = "Sort";
            this.sort.UseVisualStyleBackColor = true;
            this.sort.Click += new System.EventHandler(this.sort_Click);
            // 
            // del
            // 
            this.del.Location = new System.Drawing.Point(377, 300);
            this.del.Name = "del";
            this.del.Size = new System.Drawing.Size(122, 43);
            this.del.TabIndex = 18;
            this.del.Text = "Del";
            this.del.UseVisualStyleBackColor = true;
            this.del.Click += new System.EventHandler(this.del_Click);
            // 
            // update
            // 
            this.update.Location = new System.Drawing.Point(198, 301);
            this.update.Name = "update";
            this.update.Size = new System.Drawing.Size(123, 42);
            this.update.TabIndex = 17;
            this.update.Text = "Update";
            this.update.UseVisualStyleBackColor = true;
            this.update.Click += new System.EventHandler(this.update_Click);
            // 
            // add
            // 
            this.add.Location = new System.Drawing.Point(22, 302);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(125, 41);
            this.add.TabIndex = 16;
            this.add.Text = "Add";
            this.add.UseVisualStyleBackColor = true;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // DataSV
            // 
            this.DataSV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataSV.Location = new System.Drawing.Point(7, 83);
            this.DataSV.Name = "DataSV";
            this.DataSV.RowHeadersWidth = 62;
            this.DataSV.RowTemplate.Height = 28;
            this.DataSV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataSV.Size = new System.Drawing.Size(1051, 203);
            this.DataSV.TabIndex = 15;
            this.DataSV.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataSV_RowHeaderMouseClick);
            // 
            // search
            // 
            this.search.Location = new System.Drawing.Point(871, 39);
            this.search.Name = "search";
            this.search.Size = new System.Drawing.Size(108, 34);
            this.search.TabIndex = 14;
            this.search.Text = "Search";
            this.search.UseVisualStyleBackColor = true;
            this.search.Click += new System.EventHandler(this.search_Click);
            // 
            // search_input
            // 
            this.search_input.Location = new System.Drawing.Point(633, 44);
            this.search_input.Name = "search_input";
            this.search_input.Size = new System.Drawing.Size(206, 26);
            this.search_input.TabIndex = 13;
            // 
            // lopHp
            // 
            this.lopHp.FormattingEnabled = true;
            this.lopHp.Location = new System.Drawing.Point(108, 43);
            this.lopHp.Name = "lopHp";
            this.lopHp.Size = new System.Drawing.Size(236, 28);
            this.lopHp.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 20);
            this.label7.TabIndex = 0;
            this.label7.Text = "Lớp HP";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1149, 655);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Gender.ResumeLayout(false);
            this.Gender.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataSV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox Gender;
        private System.Windows.Forms.RadioButton Female;
        private System.Windows.Forms.RadioButton Male;
        private System.Windows.Forms.TextBox diachi;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox Phone;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxLopHp;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.TextBox mssv;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox sortProperty;
        private System.Windows.Forms.Button sort;
        private System.Windows.Forms.Button del;
        private System.Windows.Forms.Button update;
        private System.Windows.Forms.Button add;
        private System.Windows.Forms.DataGridView DataSV;
        private System.Windows.Forms.Button search;
        private System.Windows.Forms.TextBox search_input;
        private System.Windows.Forms.ComboBox lopHp;
        private System.Windows.Forms.Label label7;
    }
}

